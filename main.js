const images = document.querySelectorAll(".image-to-show");
const buttons = document.querySelector(".buttons");
const stopBtn = document.querySelector(".stop-button");
const startBtn = document.querySelector(".start-button");

let i = 0;

function showImages(){
        buttons.classList.add("active-buttons");
        images[i].classList.remove("active");
        i++;

        if(i === images.length)  i = 0; {
        images[i].classList.add("active")

    }
}


let timerId = setInterval(showImages, 3000);

stopBtn.addEventListener("click",() =>{
    clearInterval (timerId);
    stopBtn.disabled = true;
    startBtn.disabled = false;
   
})

startBtn.addEventListener("click",() =>{
    timerId = setInterval(showImages, 3000);
    startBtn.disabled = true;
    stopBtn.disabled = false;
    
})







